﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TM.Domain.Logic.DomainModel
{
    public class UserClaim : Entity
    {

        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }

        [ForeignKey("userid")]
        public User user { get; set; }

        public int userid { get; set; }

        public int updateby { get; set; }
    }
}
