﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TM.Domain.Logic.DomainModel;

namespace TM.Domain.Logic
{
    public class DomainContext:DbContext
    {
        public string ConnectionString { get; set; }


        public DomainContext(DbContextOptions<DomainContext> options)
            : base(options)
        {
            this.ConnectionString = this.Database.GetDbConnection().ConnectionString;
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Trip> Trips { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<UserClaim> UserClaims { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                .Property(p => p.UserName)
                .HasMaxLength(50).IsFixedLength();
        }
    }
}
