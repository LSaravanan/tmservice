﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic
{
    public class Vehicle : Entity
    {
        public string Name { get; set; }
        public string Make { get; set; }

        public string Model { get; set; }
    }
}
