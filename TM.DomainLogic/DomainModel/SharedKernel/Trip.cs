﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TM.Domain.Logic
{
    public class Trip : Entity
    {

        public string SourceLocation { get; set; }

        public string DestinationLocation { get; set; }

        [ForeignKey("userid")]
        public  User user { get; set; }

        public int userid { get; set; }

        public DateTime Time { get; set; }

        [ForeignKey("vehicleid")]
        public Vehicle vehicle { get; set; }

        public int vehicleid { get; set; }

        public int Status { get; set; }

        public int updateBy { get; set; }


    }
}
