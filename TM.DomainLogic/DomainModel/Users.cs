﻿using System;
using System.Collections.Generic;
using TM.Domain.Logic.DomainModel;
using TM.Domain.Logic.Repository;

namespace TM.Domain.Logic
{
    public class User: Entity
    {

        public string UserName{ get; private set; }

        public string password { get; set; }

        public IEnumerable<Trip> Trips { get; set; }

        public int UserRole { get; set; } // 3-customer, 2.Employee, 1- Admin

        public IEnumerable<UserClaim> UserClaim { get; set; }


    }
}
