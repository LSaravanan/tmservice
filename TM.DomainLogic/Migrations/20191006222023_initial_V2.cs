﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TM.Domain.Logic.Migrations
{
    public partial class initial_V2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "Users",
                newName: "password");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "Users",
                newName: "UserName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "password",
                table: "Users",
                newName: "LastName");

            migrationBuilder.RenameColumn(
                name: "UserName",
                table: "Users",
                newName: "FirstName");
        }
    }
}
