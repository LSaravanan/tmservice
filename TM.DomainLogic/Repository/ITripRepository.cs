﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic.Repository
{
    public interface ITripRepository 
    {
        void BookTrip(Trip trip);

        void CancelTrip(int tripid);

        void ConfirmTrip(int employeeid, int tripid);

        IEnumerable<Trip> ViewAllCustomerBoookedTrip(int customerid);

        IEnumerable<Trip> ViewAllEmployeeBoookedTrip(int employeeid);

        IEnumerable<Trip> ViewAllTrips();

        Trip GetTripInfobyId(int tripid);
    }
}
