﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace TM.Domain.Logic.Repository
{
    public class VehicleRepository : IVehicleRepository
    {
        private DomainContext _context;

        public VehicleRepository(DomainContext context)
        {
            _context = context;
        }

        public void Add(Vehicle vehicle)
        {
            _context.Add(vehicle);
        }

        public void Delete(int vehicleid)
        {
            var vehicle = _context.Vehicles.Where(v => v.Id == vehicleid).FirstOrDefault();
            _context.Vehicles.Remove(vehicle);
        }

        public IEnumerable<Vehicle> GetAll()
        {
            return _context.Vehicles.ToList();
        }

        public Vehicle GetVehicleById(int vehicleid)
        {
            return _context.Vehicles.Where(v => v.Id == vehicleid).FirstOrDefault();
        }

        public void Update(Vehicle vehicle)
        {

            var updatevehicle = _context.Vehicles.Where(v => v.Id == vehicle.Id).FirstOrDefault();
            updatevehicle.Make = vehicle.Make;
            updatevehicle.Model = vehicle.Model;
            updatevehicle.Name = vehicle.Name;
            _context.Vehicles.Update(updatevehicle);


        }
    }
}
