﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic.Repository
{
    public interface IUserRepository
    {
        User GetUser(string fname, string lname);
    }
}
