﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic.Repository
{
    public interface IUnitOfWork
    {
        ITripRepository Trips { get; }
        IVehicleRepository Vehicles { get; }
        void Commit();
    }
}
