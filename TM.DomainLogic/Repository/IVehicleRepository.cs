﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic.Repository
{
    public interface IVehicleRepository
    {
        void Add(Vehicle vehicle);

        void Update(Vehicle vehicle);

        void Delete(int vehicleid);

        IEnumerable<Vehicle> GetAll();

        Vehicle GetVehicleById(int vehicleid);

    }
}
