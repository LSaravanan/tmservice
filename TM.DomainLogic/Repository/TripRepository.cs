﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data.SqlClient;
using System.Data;

namespace TM.Domain.Logic.Repository
{
    public class TripRepository : ITripRepository
    {
        private DomainContext _context;

        public TripRepository(DomainContext context)
        {
            _context = context;
        }

        public void BookTrip(Trip trip)
        {
            _context.Add(trip);
        }

        public void CancelTrip( int tripid)
        {
            var trip = _context.Trips.Where(t => t.Id == tripid).FirstOrDefault();
            _context.Trips.Remove(trip);
        }

        public void ConfirmTrip(int employeeid, int tripid)
        {
            var trip = _context.Trips.Where(t => t.Id == tripid).FirstOrDefault();
            trip.Status = 1;
            trip.updateBy = employeeid;
            _context.Update(trip);
        }

        public Trip GetTripInfobyId(int tripid)
        {
            return _context.Trips.Where(t => t.Id == tripid).FirstOrDefault();
           
        }

        public IEnumerable<Trip> ViewAllCustomerBoookedTrip(int customerid)
        {
            return _context.Trips.Where(t => t.userid == customerid).ToList();
        }

        public IEnumerable<Trip> ViewAllEmployeeBoookedTrip(int employeid)
        {
            return _context.Trips.Where(t => t.userid == employeid).ToList();
        }

        public IEnumerable<Trip> ViewAllTrips()
        {
            string connetionString=string.Empty;
            
            try
            {

                return _context.Trips.ToList();
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message.ToString() + "connectionstring:" +_context.ConnectionString);
            }
        }

    }
}
