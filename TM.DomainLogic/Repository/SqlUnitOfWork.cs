﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic.Repository
{
    public class SqlUnitOfWork : IUnitOfWork
    {
        private DomainContext _context;
        private ITripRepository _tripRepository;
        private IVehicleRepository _vehicleRepository;


        public SqlUnitOfWork(DomainContext context)
        {
            _context = context;
        }

        public ITripRepository Trips
        {
            get
            {
                if (_tripRepository == null)
                {
                    _tripRepository = new TripRepository(_context);
                }
                return _tripRepository;
            }
        }

        public IVehicleRepository Vehicles
        {
            get
            {
                if (_vehicleRepository == null)
                {
                    _vehicleRepository = new VehicleRepository(_context);
                }
                return _vehicleRepository;
            }
        }

        public void Commit()
        {
            _context.SaveChanges();
        }
    }
}
