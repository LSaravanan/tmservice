﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TM.Domain.Logic
{
    public class Trip : ValueObject<Trip>
    {


        public Trip(string sLocation, string dLocation)
        {
            SourceLocation = sLocation;
            DestinationLocation = dLocation;
        }

        public string SourceLocation { get; set; }
        public string DestinationLocation { get; set; }


        protected override bool EqualsCore(Trip other)
        {
            return SourceLocation == other.SourceLocation
                    && DestinationLocation == other.DestinationLocation;
                    
        }

        protected override int GetHashCodeCore()
        {
            unchecked
            {
                int hashCode = SourceLocation.GetHashCode();
                hashCode = (hashCode * 397) ^ DestinationLocation.GetHashCode();
               // hashCode = (hashCode * 397) ^ TripDate.GetHashCode();
                return hashCode;
            }
        }
    }
}
