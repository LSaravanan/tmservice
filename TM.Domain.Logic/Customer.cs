﻿using System;

namespace TM.Domain.Logic
{
    public sealed class Customer: Entity
    {
        public string FirstName{ get; private set; }
        public string LastName { get; private set; }

        public Trip Trip { get; private set; }

        public Customer(string cfirstname, string clastname)
        {
            FirstName = cfirstname;
            LastName = clastname;
        }

        public void BookTrip(string sourceLocation, string destLocation)
        {
            Trip = new Trip(sourceLocation, destLocation);

        }

    }
}
