﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using TM.Domain.Logic;
using TM.Domain.Logic.Repository;
using TMService.API.Controllers;
using TMService.API.Model;
using TMService.API.Services;

namespace TM.Services.Test
{

    [Category("Trip-Tests")]
    [TestFixture]
    public class TripTestShould
    {
        private List<Trip> _trips;
        private TripDto _tripdto;
        private Trip _trip;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _trips = new List<Trip>()
            {
                new Trip { userid= 1, SourceLocation = "Mysore", DestinationLocation="Chennai", Status=0, Time= DateTime.Parse("10/10/2009") , vehicleid=1 },
                new Trip { userid= 1, SourceLocation = "Bombay", DestinationLocation="Chennai", Status=0, Time= DateTime.Parse("10/15/2009"), vehicleid=2 },
                new Trip { userid= 1, SourceLocation = "Mysore", DestinationLocation="Goa", Status=0, Time=DateTime.Parse("10/19/2009"), vehicleid=3 },
                new Trip { userid= 4, SourceLocation = "Delhi", DestinationLocation="Chennai", Status=0, Time=DateTime.Parse("10/20/2009"), vehicleid=1 },
                new Trip { userid= 5, SourceLocation = "Mysore", DestinationLocation="Kerela", Status=0, Time=DateTime.Parse("10/25/2009"), vehicleid=4 },
                new Trip { userid= 6, SourceLocation = "Andhara", DestinationLocation="Chennai", Status=0, Time=DateTime.Parse("10/28/2009"), vehicleid=6},

            };

            _tripdto = new TripDto() { customerid = 1, SourceLocation = "Mysore", DestinationLocation = "Chennai", Status = 0, Time = DateTime.Parse("10/10/2009"), vehicleid = 1 };
            _trip = new Trip() { userid = 1, SourceLocation = "Mysore", DestinationLocation = "Chennai", Status = 0, Time = DateTime.Parse("10/10/2009"), vehicleid = 1 };

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Trip, TripDto>().ForMember(dest => dest.customerid, opt => opt.MapFrom(src => src.userid));
                cfg.CreateMap<TripDto, Trip>().ForMember(dest => dest.userid, opt => opt.MapFrom(src => src.customerid));
                cfg.CreateMap<VehicleDto, Vehicle>();
                cfg.CreateMap<Vehicle, VehicleDto>();
            });
        }


        [Test]
        public void Customer_view_Trip()
        {
            var logger = new Mock<ILoggingService>();
            var unitofwork = new Mock<IUnitOfWork>();

            unitofwork.Setup(m => m.Trips.ViewAllCustomerBoookedTrip(1))
                .Returns(_trips.Where(f => f.userid == 1).ToList());

            var controller = new Tripcontroller(logger.Object, unitofwork.Object);
            var result = controller.GetAllCustomerTrips(1) as ObjectResult;

            var responseValue = result.Value as List<Trip>;
            Assert.IsInstanceOf<OkObjectResult>(result);
            Assert.That(responseValue.Count, Is.EqualTo(3));

        }

        [Test]
        public void Admin_view_Trip()
        {
            var logger = new Mock<ILoggingService>();
            var unitofwork = new Mock<IUnitOfWork>();

            unitofwork.Setup(m => m.Trips.ViewAllTrips())
                .Returns(_trips.ToList());

            var controller = new Tripcontroller(logger.Object, unitofwork.Object);
            var result = controller.GetAllTrips() as ObjectResult;

            var responseValue = result.Value as List<Trip>;
            Assert.IsInstanceOf<OkObjectResult>(result);
            Assert.That(responseValue.Count, Is.EqualTo(6));

        }

        [Test]
        public void Customer_Add_Trip()
        {
            var logger = new Mock<ILoggingService>();
            var unitofwork = new Mock<IUnitOfWork>();

            unitofwork.Setup(m => m.Trips.BookTrip(_trip));

            var controller = new Tripcontroller(logger.Object, unitofwork.Object);
            var result = controller.BookTrip(_tripdto) as NoContentResult;

         
            Assert.AreEqual(204, result.StatusCode);
     

        }

        [Test]
        public void Customer_Cancel_Trip()
        {
            var logger = new Mock<ILoggingService>();
            var unitofwork = new Mock<IUnitOfWork>();

            unitofwork.Setup(m => m.Trips.CancelTrip(1));

            var controller = new Tripcontroller(logger.Object, unitofwork.Object);
            var result = controller.CancelTrip(1) as NoContentResult;


            Assert.AreEqual(204, result.StatusCode);


        }

        [Test]
        public void Employee_Confirm_Trip()
        {
            var logger = new Mock<ILoggingService>();
            var unitofwork = new Mock<IUnitOfWork>();

            unitofwork.Setup(m => m.Trips.ConfirmTrip(1,2));

            var controller = new Tripcontroller(logger.Object, unitofwork.Object);
            var result = controller.ConformTrip(2,1) as NoContentResult;


            Assert.AreEqual(204, result.StatusCode);

        }


    }
}
