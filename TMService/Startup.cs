﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Serialization;
using Microsoft.AspNetCore.Mvc.Formatters;
using TMService.API.Services;
using TM.Domain.Logic;
using Microsoft.EntityFrameworkCore;
using TM.Domain.Logic.Repository;
using TMService.API.Model;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Antiforgery;

namespace TMService
{
    public class Startup
    {

        //public static IConfigurationRoot configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            //var builder = new ConfigurationBuilder()
            //    .SetBasePath(env.ContentRootPath)
            //    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            //configuration = builder.Build();
        }

        public  static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            JwtSettings settings;
            settings = GetJwtSettings();

        
            // Register Jwt as the Authentication service
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "JwtBearer";
                options.DefaultChallengeScheme = "JwtBearer";
            })
            .AddJwtBearer("JwtBearer", jwtBearerOptions =>
            {
                jwtBearerOptions.TokenValidationParameters =
          new TokenValidationParameters
          {
              ValidateIssuerSigningKey = true,
              IssuerSigningKey = new SymmetricSecurityKey(
              Encoding.UTF8.GetBytes(settings.Key)),
              ValidateIssuer = true,
              ValidIssuer = settings.Issuer,

              ValidateAudience = true,
              ValidAudience = settings.Audience,

              ValidateLifetime = true,
              ClockSkew = TimeSpan.FromMinutes(
                     settings.MinutesToExpiration)
          };
            });

            services.AddAuthorization(cfg =>
           {
               cfg.AddPolicy("canBookTrip", p => p.RequireClaim("canBookTrip", "true"));
           });

            services.AddMvc()
                .AddMvcOptions(o => o.OutputFormatters.Add(
                    new XmlDataContractSerializerOutputFormatter()));

            services.AddTransient<ILoggingService, LoggingService>();

            services.AddDbContext<DomainContext>(o => o.UseSqlServer(Startup.Configuration["ConnectionStrings:DefaultConnection"]));

            services.AddScoped<ITripRepository, TripRepository>();
            services.AddScoped<IVehicleRepository, VehicleRepository>();

            services.AddScoped<IUnitOfWork, SqlUnitOfWork>();

            services.AddAntiforgery(options => options.HeaderName = "X-XSRF-TOKEN");
            

            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IAntiforgery antiforgery)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.Use(async (context, next) =>
                {
                    if (context.Request.Path.Value.IndexOf("/api", StringComparison.OrdinalIgnoreCase) != -1)
                    {
                        var token = antiforgery.GetAndStoreTokens(context);
                        context.Response.Cookies.Append("XSRF-TOKEN",
                            token.RequestToken, new CookieOptions
                            {
                                HttpOnly = true
                            });
                    }

                    await next();
                });
            }

            

            app.UseAuthentication();

            app.UseCors(options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            app.UseStatusCodePages();


            

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Trip, TripDto>().ForMember(dest => dest.customerid, opt => opt.MapFrom(src => src.userid));
                cfg.CreateMap<TripDto, Trip>().ForMember(dest => dest.userid, opt => opt.MapFrom(src => src.customerid));
                cfg.CreateMap<VehicleDto, Vehicle>();
                cfg.CreateMap<Vehicle, VehicleDto>();
            });

           

            app.UseMvc();

                   
        }

        public JwtSettings GetJwtSettings()
        {
            JwtSettings settings = new JwtSettings();

            settings.Key = Configuration["JwtSettings:key"];
            settings.Audience = Configuration["JwtSettings:audience"];
            settings.Issuer = Configuration["JwtSettings:issuer"];
            settings.MinutesToExpiration =
             Convert.ToInt32(
                Configuration["JwtSettings:minutesToExpiration"]);

            return settings;
        }


    }
}
