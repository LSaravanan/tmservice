﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TM.Domain.Logic;
using TM.Domain.Logic.Repository;
using TMService.API.Model;
using TMService.API.Services;

namespace TMService.API.Controllers
{
    [Route("api/trip")]
    //[Authorize]
    public class Tripcontroller : Controller
    {
       
        private ILoggingService _logservice;
        private IUnitOfWork _unitofwork;

        public Tripcontroller(ILoggingService logservice, IUnitOfWork unitofwork)
        {

            _logservice = logservice;
            _unitofwork = unitofwork;
        }


        [HttpGet("")]
        public IActionResult GetAllTrips()
        {
            string msg = "Loaded Loki-";

            try
            {
                

                var trip = _unitofwork.Trips.ViewAllTrips();

                var result = AutoMapper.Mapper.Map<IEnumerable<TripDto>>(trip);

                if (trip == null)
                {
                    return NoContent();
                }

                return Ok(trip);

               

            }
            catch (Exception ex)
            {
                _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500, msg + ex.Message.ToString());
            }

        }

        [HttpGet("customer/{customerid}")]
        public IActionResult GetAllCustomerTrips(int customerid)
        {
            
            try
            {
           
                var trip = _unitofwork.Trips.ViewAllCustomerBoookedTrip(customerid);

                var result = AutoMapper.Mapper.Map<IEnumerable<TripDto>>(trip);

                if(trip == null)
                {
                    return NoContent();
                }

                return Ok(trip);

            }
            catch (Exception ex)
            {
                _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }
            
        }

        [HttpGet("employee/{employeeid}")]
        //[Authorize(Policy = "canBookTrip")]
        public IActionResult GetAllEmployeeTrips(int employeeid)
        {

            try
            {

                var trip = _unitofwork.Trips.ViewAllEmployeeBoookedTrip(employeeid);

                var result = AutoMapper.Mapper.Map<IEnumerable<TripDto>>(trip);

                if (trip == null)
                {
                    return NoContent();
                }

                return Ok(trip);

            }
            catch (Exception ex)
            {
                _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }

        }

        [HttpGet("{tripid}" , Name = "GetTripDetails")]
        public IActionResult GetTripDetails(int tripid)
        {
            try
            {

                var trip = _unitofwork.Trips.GetTripInfobyId(tripid);

                var result = AutoMapper.Mapper.Map<TripDto>(trip);

                if (trip == null)
                {
                    return NoContent();
                }

                return Ok(trip);

            }
            catch (Exception ex)
            {
                _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }
        }

        [HttpPost("")]
        public IActionResult BookTrip([FromBody] TripDto trip)
        {
            try
            {
                var result = AutoMapper.Mapper.Map<Trip>(trip);

                _unitofwork.Trips.BookTrip(result);
                _unitofwork.Commit();

                return NoContent();

            }
            catch (Exception ex)
            {
                _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }

        }

        [HttpPut("{tripid}/employee/{employeeid}")]
        public IActionResult ConformTrip(int tripid, int employeeid)
        {
            try
            {
                //var result = AutoMapper.Mapper.Map<Trip>(trip);

                _unitofwork.Trips.ConfirmTrip(employeeid, tripid);
                _unitofwork.Commit();

                return NoContent();

            }
            catch (Exception ex)
            {
                _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }

        }

        [HttpDelete("{tripid}")]
        public IActionResult CancelTrip(int tripid)
        {
            try
            {
                
                _unitofwork.Trips.CancelTrip(tripid);
                _unitofwork.Commit();

                return NoContent();

            }
            catch (Exception ex)
            {
                _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }

        }

    }
}
