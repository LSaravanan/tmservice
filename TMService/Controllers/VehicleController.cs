﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TM.Domain.Logic;
using TM.Domain.Logic.Repository;
using TMService.API.Model;
using TMService.API.Services;

namespace TMService.API.Controllers
{
    [Route("api/vehicle")]
    [Authorize]
    [ValidateAntiForgeryToken]
    public class VehicleController : Controller
    {
   
        private IUnitOfWork _unitofwork;
        private ILoggingService _logservice;

        public VehicleController(ILoggingService logservice, IUnitOfWork unitofwork)
        {
            _unitofwork = unitofwork;
            _logservice = logservice;
        }

        [HttpGet("")]
        public IActionResult GetAllVehicles()
        {
            try
            {

                var vehicles = _unitofwork.Vehicles.GetAll();

                var result = AutoMapper.Mapper.Map<VehicleDto>(vehicles);

                if (vehicles == null)
                {
                    return NoContent();
                }

                return Ok(result);

            }
            catch (Exception ex)
            {
               _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }

        }

        [HttpGet("{id}")]
        public IActionResult GetVehicleById(int id)
        {
            try
            {
                var vehicle = _unitofwork.Vehicles.GetVehicleById(id);

                var result = AutoMapper.Mapper.Map<VehicleDto>(vehicle);

                if (vehicle == null)
                {
                    return NoContent();
                }

                return Ok(result);

            }
            catch(Exception ex)
            {
               _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }
        }

        [HttpPost("")]
        public IActionResult AddVehicle(VehicleDto vehicle)
        {
           

            try
            {

                var result = AutoMapper.Mapper.Map<Vehicle>(vehicle);

                _unitofwork.Vehicles.Add(result);
                _unitofwork.Commit();

                return NoContent();

            }
            catch (Exception ex)
            {
               _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }

        }

        [HttpPut("")]
        public IActionResult UpdateVehicle(VehicleDto vehicle)
        {
            
            try
            {
                var result = AutoMapper.Mapper.Map<Vehicle>(vehicle);
                _unitofwork.Vehicles.Update(result);
                _unitofwork.Commit();

                return NoContent();
            }
            catch (Exception ex)
            {
               _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }

        }

        [HttpDelete("")]
        public IActionResult DeleteVehicle(int id)
        {
            

            try
            {
                _unitofwork.Vehicles.Delete(id);
                _unitofwork.Commit();

                return NoContent();

            }
            catch (Exception ex)
            {
               _logservice.Log("Exception", ex.Message, "Critical");
                return StatusCode(500);
            }

        }

    }
}
