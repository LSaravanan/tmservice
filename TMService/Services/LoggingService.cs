﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using TMService.API.Model;

namespace TMService.API.Services
{
    public class LoggingService : ILoggingService
    {
        static HttpClient client = new HttpClient();

        public void Log(string description, string message, string additionalInformation)
        {
            // Update port # in the following line.
           // client.BaseAddress = new Uri("http://localhost:59857");// we need to add this to dependency IOC and pass the url from the json config.
           // client.DefaultRequestHeaders.Accept.Clear();
           // client.DefaultRequestHeaders.Accept.Add(
           //     new MediaTypeWithQualityHeaderValue("application/json"));


           // await LogInformationAsync(new LogDto(){ Descriptoin= description, Message= message, AdditionalInformation= additionalInformation });

           //// Need to call another microservice to log
           // Debug.WriteLine($"Description: {description}. Exception Details: {message}. Log Level: {additionalInformation}");
           
        }

        static async Task<Uri> LogInformationAsync(LogDto log)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "api/log", log);
            response.EnsureSuccessStatusCode();

            return response.Headers.Location;
        }
    }
}
