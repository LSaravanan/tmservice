﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TMService.API.Model
{
    public class VehicleDto
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Make { get; set; }

        public string Model { get; set; }
    }
}
