﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TMService.API.Model
{
    public class LogDto
    {
        public string Descriptoin { get; set; }

        public string Message { get; set; }

        public string AdditionalInformation { get; set; }
    }
}
