﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TMService.API.Model
{
    public class TripDto
    {
        public int Id { get; set; }

        public string SourceLocation { get; set; }

        public string DestinationLocation { get; set; }

        public int customerid { get; set; }

        public DateTime Time { get; set; }

        public int vehicleid { get; set; }

        public int Status { get; set; }

        public int updateby { get; set; }
    }
}
