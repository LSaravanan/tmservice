#Build Stage
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 as build-env

WORKDIR /tmservice

#copy
COPY . .

#restore
RUN dotnet restore

#test
RUN dotnet test TM.Services.Test/TM.Services.Test.csproj

#publish
ENV TEAMCITY_PROJECT_NAME=fake
RUN dotnet publish -o /publish


# Runtime Stage
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /publish
COPY --from=build-env /publish /publish
ENTRYPOINT [ "dotnet", "TMService.API.dll" ]